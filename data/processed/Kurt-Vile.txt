   Wakin, the dawn of day And I gotta think about What I wanna say Phone ringing off the shelf I guess it wanted to kill himself Wakin on a pretty day Don't know why I ever go away It's hard to explain My love, in this daze   You could say I been most all around But honey I ain't goin nowhere Don't worry bout a thing It's only dying I live along a straight line Nothing always comes to mind To be frank, I'm fried But I don't mind  Yeah, yeah Yeah, yeah Yeah, yeah, yeah  Yeah, yeah Yeah, yeah Yeah, yeah, yeah  Yeah, yeah Yeah, yeah Yeah, yeah, yeah  Yeah, yeah Yeah, yeah Yeah, yeah, yeah   Been diggin layin low, low, low I'm diggin layin low, low, low Dig, dig in... To these lives that we are living in Living low, lackadaisically so   Risin at the crack of dawn And I gotta think about What wisecracks I'm going to drop along the way  Phone ringin off the shelf I guess somebody had something they Really wanted to prove to us  Yeah, I'm waking on a pretty day Floating in place, no need sayin nothin To explain it to my loved ones Today  Yeah, yeah, yeah  Yeah, yeah Yeah, yeah Yeah, yeah, yeah  Yeah, yeah Yeah, yeah Yeah, yeah, yeah  Yeah, yeah Yeah, yeah Yeah, yeah, yeah  Yeah, yeah Yeah, yeah Yeah, yeah, yeah  Yeah, yeah Yeah, yeah Yeah, yeah, yeah  Yeah, yeah Yeah, yeah Yeah, yeah, yeah  Yeah, yeah Yeah, yeah Yeah, yeah, yeah  Yeah, yeah Yeah, yeah Yeah, yeah, yeah  
   I should have known My heart has overgrown "Do you risk it exploding all over?" Well you better get a load of this one With a load on The time has come And I believe I've come too For to tell you But something's always in the way   All right, what now? That's fine, I think I'm ready To claim what's mine Rightfully, yeah   Crestfallen Dejected with the East coast crutch Day dreaming through the dark days Club mate on holiday With the Moon Duo Space partners   All right, what now? That's fine, I think I'm ready To claim what's mine Rightfully, yeah   The time has come And i think i came too Bleeding, bleeding Ever so slightly, yeah   I could have called this Saw it a mile away "Baby, stay."  
   Spirit in the sky Transparent to the eye Talking trash on nothing the human eye can see anyway And it's anarchy for you and me When all this work is out of mind  There was a time in my life when they thought I was all talk Now I'm feeling stalked by God walking I got the other hint Walking   Now take a look at my hands Watch me go Watch me going, yeah I'm going, yeah I'm gone  Freak in the fog Through the dew in the dawn All the words I'm saying are wrong, now won't you watch me? And yet there was a time in my life when they thought I was all talk  Left-handed smoke shifting From my right hand man Can you sense the snag in my playing?  Spirit in the ether From a right wing sister Safe to say she don't care that much  There was a time in my life that is gone there's some time still go back to Not to be all stalking myself too much Then I got the other hint, here, in present days   Now take a look at my hands Watch me go Watch me going, yeah I'm going, yeah I'm gone  Freak in the fog In the sea of the stone Making music is easy, watch me And yet there was a time in my life when they thought I was all  
   In the dawn observing light I recall a girl named Alex She and Mark they where happily wed Hey at least in my head   I think about them all the time I think about them all the time I think about them all the time I think about them all the time   I wanna live all the time In my fantasy infinity There I will never be abandoned There I'll ever handle against everything from ever happening to them   And they hate it when I say I wanna   I wanna walk out into the night Without it being running away From a bad day in my brain For the sake of this drift that I could be cruising In the comfort of a sportscar illusion   I think about them all the time I think about them all the time I think about them all the time I think about them all the time  They hate it when I say  I think about them all the time I think about them all the time I think about them all the time I think about them all the time I think about them all the time I think about them all the time I think about them all the time I think about them all the time I think about them all the time I think about them all the time I think about them all the time I think about them all the time  
   You wedded on my day, you wedded on You wedded on my day Each morning we marry it's just the most gorgeous of days I know you'll never run away   Your met a young man who was a wild child Who harmonized his keys in his droning mind Saxophones sing from inside his head crying   I know you'll never run away I know you'll never run away I know you'll never run away I know you'll never run away   I'm living all the time thanks cause you're mine You turn my dying days away Each day we carry on like believers and lovers Though there are others who would rather run away   Ay ay ay, ay ay ay now If you wanna hear me sing press play Ay ay ay, ay ay ay man Pay ay ay close attention   I know you'll never run away I know you'll never run away I know you'll never run away I know you'll never run away   I'm holding you all night through to the dawn You turn my dying days away My steady diet of high on love Then taking dope to cope, have to come down sometime   Ay ay ay, ay ay ay now Ay ay ay, ay ay ay man Pay ay ay close attention Ay ay ay, ay ay ay now   I know you'll never run away I know you'll never run away I know you'll never run away I know you'll never run away I know you'll never run away I know you'll never run away I know you'll never run away I know you'll never run away   You wedded on my day, you wedded on You wedded on my day Each morning we marry it's just the most gorgeous of days I know you'll never run away  
   Well I want to be with you  I don't know, well I'm workin Babe, but I want you, too, there But I want to be around And I want you around, deep down Imbedded in my brain Inbedded in my-wow What pure pain    All of the roads that I travel down Get lost and then found again All bring me back to my baby Yeah, that's what you are Every time that I look out my window All of my thoughts they go travelin Out where the coldest of winds blow Only to drift back to you, girl Yeah, that's what you are Yeah, that's what you are   And I want to be with you  I don't know, when I'm working, babe, I want/wish you were here Cause I want to be around And I want you around Deep down I'm breaking down Wow, it's the purest of pure pain   Every time that I look out my window All my emotions they are spreading Zip through winding highways in my head Pick up momentum then I'm coasting Only to turn around abrupt Come back for my love Yeah, that's what you are Yeah, that's what you are  
  Take your time So they say and that's probably The best way to be, but what about Those who are fathers and What about their daughters? I will promise to do my very best To do my duty For God and my country Hey but I'm just human after all I will promise not to smoke too much and I will promise not to party Too hard... too hard... too hard...   Did you ever bang on a xylophone That took you everywhere from home? Well I did... Well I've tried... Life is like a ball of beauty that Makes you wanna just cry, then you die  I will promise to go outta my way To do the right thing from now on  There comes a time in every man's life When he's gotta take hold of the hand That ain't his but it is Take your time, so they say, and that's Gotta be the best way, but what about Those who are fathers and What about the others? I know that you don't know a stranger But I'm no stranger than the rest And I'm no stranger to you  Take your time they say it's all around And I think I'd say I know Just where that I am bound There comes a time in every man's life When he's gotta hold tight To the heart of the matter at hand So take your time, baby girl Cause that's, that's the best of all  I will promise to do my very best To do my very best for you And that won't be Too hard... too hard... too hard... Too hard...  
  Everyone's saying I should probably give up And hey, I wouldn't wanna waste no time How can I even look myself in the mirror Then again, why would I?  It's just another day in the shame chamber Living life to the lowest power Feeling bad in the best way a man can  Shame on you, shame on you Shame on you Oh baby when you cry, it brings a tear to my eye Oh shame on us  Chubby in the face in a world of muck and slime Time for the comedown How can I even look myself in the mirror? Then again I guess I think I  Wouldn't really mind in the shame chamber Living life to the lowest power Feeling bad in the best way a man can  Yeah, it's just another day in the shame chamber Living life to the lowest power Feeling bad in the best way a man can  Shame on you, shame on you Shame on you Oh baby when you cry, it brings a tear to my eye Oh shame on us  
   Bought a ticket from a tin man, he was my main man He told me something, he told me everything Through my dark day, he led the way Again I stray away, again I stay   There is but one true love, within my heart There is but one man, that I am When I'm away out there, I wanna go home When I am home, my head stays out there   Snowflakes are dancing, discman is pumping Headphones are loud, chilling on a pillowy cloud Comfort of codeine, and Springsteen pristine You should sing just whatever  Ooh Ooh Ooh  
  Strange, strange days inside My mind is daily changing And it's charged by The shifting tones of time flying Just in case you were wondering where I Was Was Strange Strange, strange days inside My mind is daily changing As result of the black cloud coming down Just in case you were watching the Horizon Horizon Horizon      Strange, strange days inside  
  Sometimes when I get in my zone, you'd think I was stoned But I never as they say, touched the stuff  I might be adrift, but I'm still alert Concentrate my hurt into a gold tone  Golden tones  In the night when all hibernate, I stay awake Searching the deep, dark, depths of my soul tone  Golden tones Gold tones  There's a place in my heart for all of my friends Some have stepped out but some check back in  Been livin' my life on the run, from day one Every day is "hey, so are they"  But if I been lookin' back today Looking back in gold tones  Yeah, yeah  In the night when all hibernates I stay awake Searching the deep, dark, depths of my soul tone Golden tones  Yeah, yeah  I been all around, been up and down This round world that is always turning  Til some day or so they say When a-hey, hey, I wanna hear a gold tone  Sometimes as I'm floating away I wish I could stay Then I arrive among the dropping flies  There's a place in my heart, been trying to locate It's somewhere within and it's for all them with its gold tones  Yeah, yeah  In the night when all hibernates I stay awake Searching the deep, dark depths of my soul tone Golden tones  Sometimes when I get in my zone, you'd think I was stoned But I never as they say, touched the stuff  I might be adrift, but I'm still alert Concentrate my hurt into a gold tone  Yeah, yeah  
  There has been but one true love In my baby’s arms, in my baby’s arms And I got the hands to hold onto them  I get sick of just about everyone And I hide in my baby’s arms My baby’s arms Cause except for her, you know, as I've implied  I will never ever ever be alone Cause it’s all in my baby’s hands Shiny, shiny secret stones In my baby’s hands In my baby’s hands  I get sick of just about everyone And I hide in my baby’s arms Shrink myself just like a Tom Thumb And I hide in my baby’s hands Hide in my baby’s hands  Cause except for her There just ain't nothing to latch onto  There has been but one true love In my baby’s arms In my baby’s arms In my baby’s arms In my baby’s arms In my baby’s arms In my baby’s arms In my baby’s arms  
  I'd pack my suitcase with myself But I'm already gone Cleanse myself with vitamin health But I'm already gone  I saw it rising through the horizon And I saw it fall The Jesus fever's falling all over You believers and lovers  In a black hole I found a broken skull Now I'm already gone You can write my whole life down in a little book When I'm already gone  I started shaking and my heart breaking And my belly crawls The Jesus fever's falling all over You believers and lovers  When I'm a ghost I see no reason to run When I'm already gone If it wasn't taped, you could escape this song But I'm already gone  
  Well I think by now you probably think I am a puppet to the man Well, I'll tell you right now you best believe that I am Sometimes I'm stuck in and I think I can unglue it Sometimes I'm stuck in and I think I can unglue it  This one goes out to all those Who want to rap to survive Enough comes by saying, looting I want 'em to die My sister's stuck and She don't think she can unglue it Sometimes I get stuck in a rut too It's okay girlfriends Sometimes I been rough so much I want it to end  Well I think by now you probably think I am a puppet to the man But I shout it out loud because I now that I am Sometimes I'm stuck in and I think I can unglue it Will you help me do it, will you help me do it?  Will you help me do it, will you help me do it? Will you help me do it, will you help me do it? Will you help me do it, will you help me do it? Will you help me do it, will you help me do it?  
  On tour Lord of the flies Aw, hey, who cares? What's a guitar?  Watch out for this one He'll stab you in the back for fun I'm just playin' I know you, man Most of the time  Watch out for this one He'll pump you full of lead for turnin' your head wrong I would know I see through everyone, even my own self now  I wanna write my whole life down Burn it there to the ground I wanna sing at the top of my lungs For fun, screaming annoyingly Cause that's just me Being me, being free  Watch out for this one He'll pump you full o' lead for turnin' your head wrong I would know Cause I can see through him and them  On tour Lord of the flies I'm just playin' I got it made Most of the time  I wanna beat on a drum so hard 'Til it bleeds blood Pull out the heart 'Til it don't start again  
  Society is my friend He makes me laugh in a cool blood bay Society is my friend He makes me laugh down in a cool blood bay  Oh, society Oh, yeah  Society is my friend He makes me laugh down in a cold blood bay, yeah He stole my lady's hand Hey how you doin' this kids My hand is not for you to hold So kiss me with your mouth without closing it all that much  Society is all around Are you the beautiful sound of all the half pip-squeal Ecstatic brilliance at it's finest, that's my friend Society is all around It takes me down  Society is my friend He makes me laugh down in a cold blood bay, yeah Society is my friend He makes me laugh down, down  
   Hey old man how many times we gotta tell ya? We don’t want none but where you been so long? Hey girl come on over That’ll be just fine   If it ain’t working take a whiz on the world An entire nation drinking from a dirty cup My best friend’s long gone but I got runner ups yeah   When I’m walking my head is practically dragging Yeah and all I ever see is just a whole lot of dirt My whole life’s been one long running gag Two packs of red apples for the long ride home Well you know baby   Sick of walking so you took a wrong way train Then you sat down and couldn’t get up My best friend’s long gone but I got runner ups yeah My best friend’s long gone but I got runner ups   I don’t know if it’s real but it’s how I feel Don’t know if you really came but I feel dumb in asking You should've been an actress you're so domineering Take two white gold earrings for your troubles, ma'am   When it’s looking dark punch the future in the face Instead of standing I’m running around The sharpest tool in school don’t even know what’s up My best friend’s long gone but I got runner ups, yeah  
  In my day I was young and crazy Sure I didn't know shit, but now I'm lazy One day I won't even know what was better Then again and now I want much of nothing anyway  Two of us, one on each shoulder, I'm tryin' to turn We're pullin' over, on this shoulder, ain't driving I know when I get older, I'm dyin' Well, I got everything I need and now And it's fine now, it's fine now, it's fine  In my time I was whack and wild I was just being myself then But then I sat there just tryin' on faces Wanna erase how discreet disgrace is  Pulling over on the shoulder, ain't driving Put two of us, one on each shoulder, I'm tryin' to turn I know when we get older, I'm dyin' Well, I got everything I need and now And it's fine now, it's fine now, it's fine  
   I don't want to change but I don't want to stay the same I don't want to go but I'm running I don't want to work but I don't want to sit around All day frowning   I don't want to give up but I kinda want to lie down but not sleep just rest Give me a break how much does it really take? Get my head out of here   I been searching I don't know what for I came across some girl She was a tomboy   And I was a peeping tom More than it seems I was a peeping tom You know what I mean I was a I was a I was a I was a   Now I want to go but its a one way street with me So I've been told and I'm going When I'm down I would never come around But you should be kind and read my mind   I been searching I don't know what for She was a tomboy And I admired her   Cause I was a peeping tom More than it seems I was a peeping tom You know what I mean I was a I was a I was a I was a   I don't want to change but I don't want to stay the same I don't want to go but I'm running I don't want to work but I don't want to sit around All day frowning  
   Smoke ring for my halo Angel demon human Overestimated Over and over  Think I can see it now Think I can see it now Think I can see it now Think I can see it now   Smoke ring for my halo Human angel demon Underestimated Right under the roof of your house  Thought you could see it then Thought you could see it then Thought you could see it then Thought you could see it then   Sipping from the sewer can Exercises my hand Pacifies the land Makes the most out of your chill time man  I feel like laying down I feel like laying down Yeah and I want to do it again Oh what a mess I guess I'm in  
  In the morning I'm not done sleeping In the evening I guess I'm alive It's alright to still peel myself up sleepwalking In a ghost town Think I'll never leave my couch again 'Cause when I'm out I'm only out of my mind Then again I guess it ain't always that way  In the morning I'm not done sleeping In the evening I guess I'm alive It's alright to still peel myself up sleepwalking In a ghost town Think I'll never leave my couch again 'Cause when I'm out I'm only out of my mind Christ was born I was there You know me I'm around I got friends Hey wait where was I? Well I am trying  When I'm driving I found I'm dreaming Dreamin tunes and drifting It's just that sometimes I want to pullover Open up and stay in Raindrops might fall on my head sometimes But I don't pay 'em any mind Christ was here you just missed him Now I'm out going down second To all the stores to see my friends To find cakes, to find old ships  When I'm drinking I get to joking Then I'm laughing falling down Well that's just fine and I just pick myself up and walk down Ghost town Raindrops might fall on my head sometimes But I don't pay 'em any mind Then again I guess it ain't always that way  
