    
  John Coltrane - tenor saxophone Lee Morgan - trumpet Curtis Fuller - trombone Kenny Drew - piano Paul Chambers - bass Philly Joe Jones - drums  
  John Coltrane - tenor saxophone Lee Morgan - trumpet Curtis Fuller - trombone Kenny Drew - piano Paul Chambers - bass Philly Joe Jones - drums  
  John Coltrane - tenor saxophone Lee Morgan - trumpet Curtis Fuller - trombone Kenny Drew - piano Paul Chambers - bass Philly Joe Jones - drums  
    
    
    
    
    
    
    
  Raindrops on roses and Whiskers on kittens Bright copper kettles and Warm woolen mittens Brown paper packages Tied up with strings These are a few of My favorite things Cream colored ponies and Crisp apple strudels Door bells and sleigh bells And schnitzel with noodles Wild geese that fly with The moon on their wings These are a few of My favorite things Girls in white dresses With blue satin sashes Snowflakes that stay on My nose and eyelashes Silver white winters That melt into springs These are a few of My favorite things When the dog bites When the bee stings When i'm feeling sad I simply remember My favorite things And then i don't feel so bad  
  Summertime and the livin' is easy Fish are jumpin' and the cotton is fine Oh your Daddy's rich and your ma is good lookin' So hush little baby, don't you cry  One of these mornings You're goin' to rise up singing Then you'll spread your wings And you'll take the sky But till that morning There's a nothin' can harm you With daddy and mammy standin' by  
  Old man sunshine listen you Never tell me dreams come true Just try it and I'll start a riot Beatrice Fairfax don't you dare Ever tell me he will care I'm certain it's the final curtain I never want to hear from any cheerful Pollyannas Who tell you fate supplies a mate It's all bananas They're writing songs of love but not for me A lucky star's above but not for me With love to lead the way I've found more clouds of gray Than any Russian play could guarantee I was a fool to fall and get that way Hi-ho, alas, and also lack-a-day Although I can't dismiss the memory of his kiss I guess he's not for me I was a fool to fall and get that way Hi-ho, alas, and also lack-a-day Although I can't dismiss the memory of his kiss  
    
